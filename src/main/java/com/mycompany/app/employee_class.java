package employee_class;


/* Override equals(),toString() in Employee & Account class */


public class employee_class {
	
	int empid;
	String empname,dept;
	
	@Override
	public boolean equals(Object o) 
	{
	    if (o instanceof employee_class) 
	    {
	      employee_class c = (employee_class) o;
	      if (( this.empid == c.empid)&&(this.dept == c.dept)&&(this.empname == c.empname) ) 
	         return true;
	    }
	    return false;
	}
	
	public String toString(employee_class p)
	{
		return p.getdeptname()+','+p.getempid()+','+p.getempname() ;
	}
	
	public employee_class(int empid, String dept, String empname)
	{
		this.empid = empid;
		this.dept = dept;
		this.empname = empname;
		
	}
		public int getempid()
		{
			return empid;
		}
		
//	public void setempid(int empid)
//	{
//		this.empid = empid;
//	}

	
	public String getempname()
	{
		return empname;
	}
	
/*	public void setempname(String empname)
	{
		this.empname = empname;
	}*/
	public String getdeptname()
	{
		return dept;
	}
	
/*	public void setdeptname(String dept)
	{
		this.dept = dept;
	}*/
	 void display(){System.out.println(getempid()+" "+getempname()+" "+getdeptname());}  
	

public static void main(String [] args)
{
	employee_class emp = new employee_class(1,"CSE","bunny2");
	employee_class emp1 = new employee_class(2,"ECE","bhargav");
	employee_class emp2 = new employee_class(2,"ECE","bhargav");
    emp.display();
    emp1.display();
    emp2.display();
   System.out.println("\nObject comparision");
    if(emp1.equals(emp2))
    {
    	System.out.println("equals\n");
    }
    else
    {
    	System.out.println("not equals\n");
    }
  /*  if(emp.equals(emp2))
    {
    	System.out.println("equals\n");
    }
    else
    {
    	System.out.println("not equals\n");
    }*/
    System.out.println("display using toString method");
	System.out.println(emp.toString(emp)); //display using toString method
	
}
}

